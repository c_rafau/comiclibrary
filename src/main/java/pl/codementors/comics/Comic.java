package pl.codementors.comics;

import java.io.Serializable;

/**
 * Representation of single comics in library. Describes comics title, author, cover type, publish year and month.
 *
 * @author psysiu
 */
public class Comic implements Serializable {

    /**
     * Describes comics cover type.
     */
    public enum Cover {
        HARD,
        SOFT;
    }

    /**
     * Comic title.
     */
    private String title;

    /**
     * Comic author.
     */
    private String author;

    /**
     * Comics series.
     */
    private String series;

    /**
     * Publish year. Must be positive integer from 1867 (date of first serialized comics for a mass audience)
     * - 2017 range.
     */
    private int publishYear;

    /**
     * Publish month. Must be positive integer from 1 - 12 range.
     */
    private int publishMonth;

    /**
     * Comic cover.
     */
    private Cover cover;

    /**
     * Default constructor. Sets title and author to empty strings, publish year to 1867,
     * publish month to 1 and cover to soft.
     */
    public Comic() {
        this.title = "";
        this.author = "";
        this.publishYear = 1867;
        this.publishMonth = 1;
        this.cover = Cover.SOFT;
    }

    /**
     * Creates new comics. If publish year is not in range 1867 - 2017, the 1867 value is used. If publish month
     * is not in range 1 - 12, the 1 value is used.
     *
     * @param title Comic title.
     * @param author Comic author.
     * @param cover Comic cover.
     * @param series Comics series.
     * @param publishYear Comic publish year (1867 - 2017).
     * @param publishMonth Comic publish month (1 - 12).
     */
    public Comic(String title, String author, String series, Cover cover, int publishYear, int publishMonth) {

        // reuse setters
        setPublishYear(publishYear);
        setPublishMonth(publishMonth);

        this.title = title;
        this.author = author;
        this.cover = cover;
        this.series = series;
    }

    /**
     * @return Comic title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title New value for comics title.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return Comic publish year.
     */
    public int getPublishYear() {
        return publishYear;
    }

    /**
     * Sets new value for publish year. If new value is not in range 1867 - 2017 it does nothing.
     *
     * @param publishYear New value for comics publish year.
     */
    public void setPublishYear(int publishYear) {
        if(publishYear > 2017 || publishYear < 1867) {
            this.publishYear = 1867;
        } else {
            this.publishYear = publishYear;
        }
    }

    /**
     *
     * @return Comic publish month.
     */
    public int getPublishMonth() {
        return publishMonth;
    }

    /**
     * Sets new value for publish month. If new value is not in range 1 - 12 it does nothing.
     * @param publishMonth New value for comics publish month.
     */
    public void setPublishMonth(int publishMonth) {
        if(publishMonth > 12 || publishMonth < 1) {
            this.publishMonth = 1;
        } else {
            this.publishMonth = publishMonth;
        }
    }

    /**
     *
     * @return Comic author.
     */
    public String getAuthor() {
        return author;
    }

    /**
     *
     * @param author New value for comics author.
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     *
     * @return Comic cover.
     */
    public Cover getCover() {
        return cover;
    }

    /**
     *
     * @param cover New value for comics.
     */
    public void setCover(Cover cover) {
       this.cover = cover;
    }

    /**
     *
     * @return Comic series.
     */
    public String getSeries() {
        return series;
    }

    /**
     *
     * @param series New value for comic series.
     */
    public void setSeries(String series) {
        this.series = series;
    }


    /**
     * Compares two comics
     * @param o comic to be compared
     * @return true if two comics have equal values of all fields
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comic comic = (Comic) o;

        if (publishYear != comic.publishYear) return false;
        if (publishMonth != comic.publishMonth) return false;
        if (!title.equals(comic.title)) return false;
        if (!author.equals(comic.author)) return false;
        if (!series.equals(comic.series)) return false;
        return cover == comic.cover;
    }

    /**
     * Generates hash code
     * @return generated hash
     */
    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + author.hashCode();
        result = 31 * result + series.hashCode();
        result = 31 * result + publishYear;
        result = 31 * result + publishMonth;
        result = 31 * result + cover.hashCode();
        return result;
    }
}
