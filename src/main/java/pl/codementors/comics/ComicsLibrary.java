package pl.codementors.comics;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.*;
import java.sql.Connection;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Represents comics library. Contains inner collection with comics, exposes methods for adding and removing comics.
 * Allows for changing covers of all contained comics.
 *
 * @author psysiu
 */
public class ComicsLibrary {

    /**
     * Set of comics contained in the library.
     */
    private Set<Comic> comics = new HashSet<>();

    /**
     *
     * @return All comics in the library. The returned collection is unmodifiable so it can not be changed
     * outside the library.
     */
    public Collection<Comic> getComics() {
        return Collections.unmodifiableSet(comics);
    }

    /**
     * Adds comic to the library. If comic is already in the library does nothing. If comic is null does nothing.
     *
     * @param comic Comic to be added to the library.
     */
    public void add(Comic comic) {
        if(comics.contains(comic) || comic == null) {
            return;
        } else {
            comics.add(comic);
        }
    }

    /**
     * Removes comic from the library. If comics is not present in the library does nothing.
     *
     * @param comic Comic to be removed from the library.
     */
    public void remove(Comic comic) {
        if(comics.contains(comic)) {
            comics.remove(comic);
        }
    }

    /**
     * Changes covers of all comics in the library.
     *
     * @param cover Cover type for all comics in the library.
     */
    public void changeCovers(Comic.Cover cover) {

        comics.forEach( c -> c.setCover(cover));
    }

    /**
     *
     * @return All authors of all comics in the library. Each author is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getAuthors() {
        Set<String> authors = new HashSet<>();
        comics.forEach(c -> authors.add(c.getAuthor()));

        return Collections.unmodifiableSet(authors);
    }

    /**
     *
     * @return All series of all comics in the library. Each series is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getSeries() {
        Set<String> series = new HashSet<>();
        comics.forEach(c -> series.add(c.getSeries()));

        return Collections.unmodifiableSet(series);
    }

    /**
     * Loads comics from file. Method uses FileReader and Scanner for reading comics from file.
     *
     * The file structure is:
     * number_of_comics (one line with one number, nextInt())
     * comics title (one line with spaces, nextLine())
     * comics author (one line with spaces, nextLine())
     * comics series (one line with spaces, nextLine())
     * cover (one line with one word, next())
     * publish_month (one line with one number, nextInt())
     * publish_year (one line with one number, nextInt())
     *
     * The proper sequence for reading file is to call nextInt(); skip("\n"); to read number of comics.
     * Then in loop call nextLine(); nextLine(); nextLine(), next(); nextInt(); nextInt(); skip("\n").
     *
     * If file does not exists, or is directory, or can not be read, method just ignores it and does nothing.
     *
     * @param file File from which comics will be loaded.
     */
    public void load(File file) {
        if(!file.exists() || file.isDirectory() || !file.canRead()) {
            return;
        }

        try {
            int number_of_comics;
            String title;
            String author;
            String series;
            String cover;
            int publish_month;
            int publish_year;
            Scanner in = new Scanner(new FileReader(file));

            if(in.hasNextInt()) {
                number_of_comics = in.nextInt();
                in.skip("\n");
            } else {
                return;
            }

            in.skip("\n");
            for(int i = 0; i < number_of_comics; i++) {
                title = in.nextLine();
                author = in.nextLine();
                series = in.nextLine();
                cover = in.next();
                publish_month = in.nextInt();
                publish_year = in.nextInt();
                in.skip("\n");

                Comic.Cover cvr = (cover.equals("SOFT") ? Comic.Cover.SOFT : Comic.Cover.HARD);
                comics.add(new Comic(title, author, series, cvr, publish_year, publish_month));

            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
            e.printStackTrace();
        }
    }

    /**
     * Counts all comics with the same provided series name.
     *
     * @param series Name of the series for which comics will be counted.
     * @return Number of comics from the same provided series.
     */
    public int countBySeries(String series) {
        return (int)comics.stream().filter(c -> c.getSeries().equals(series)).count();
    }

    /**
     * Counts all comics with the same provided author.
     *
     * @param author Author for which comics will be counted.
     * @return Number of comics with the same author.
     */
    public int countByAuthor(String author) {
        return (int)comics.stream().filter(c -> c.getAuthor().equals(author)).count();
    }

    /**
     * Counts all comics with the same provided publish hear.
     *
     * @param year Publish year for which comics will be counted.
     * @return Number of comics from the same provided publish year.
     */
    public int countByYear(int year) {
        return (int)comics.stream().filter(c -> c.getPublishYear() == year).count();
    }

    /**
     * Counts all comics with the same provided publish hear and month.
     *
     * @param year Publish year for which comics will be counted.
     * @param month Publish mnt for which comics will be counted.
     * @return Number of comics from the same provided publish year and month.
     */
    public int countByYearAndMonth(int year, int month) {
        int count = 0;
        for(Comic comic: comics) {
            if (comic.getPublishYear() == year && comic.getPublishMonth() == month) {
                count++;
            }
        }

        return count;
    }

    /**
     * Removes all comics with publish year smaller than the provided year. For the removal process
     * method uses iterator.
     *
     * @param year Provided year.
     */
    public void removeAllOlderThan(int year) {
        Iterator<Comic> iterator = comics.iterator();
        while(iterator.hasNext()) {
            Comic setElement = iterator.next();
            if(setElement.getPublishYear() < year) {
                iterator.remove();
            }
        }

        // java 8
        // comics.removeIf(setElement -> setElement.getPublishYear() < year);
    }

    /**
     * Removes all comics with publish year bigger than the provided year. For the removal process
     * method uses java 8 lambda.
     *
     * @param year Provided year.
     */
    public void removeAllNewerThan(int year) {
        // java 8
        comics.removeIf(setElement -> setElement.getPublishYear() > year);
    }

    /**
     * Removes all comics with publish year and publish months equals to those provided as argyments
     * @param year Provided year
     * @param month Provided month
     */
    public void removeAllFromYearAndMonth(int year, int month) {
        // java 8
        comics.removeIf(setElement -> setElement.getPublishYear() == year
                && setElement.getPublishMonth() == month);
    }
    /**
     * Removes all comics written by the specified author. For the removal process method uses iterator.
     *
     * @param author Provided author.
     */
    public void removeAllFromAuthor(String author) {
        Iterator<Comic> iterator = comics.iterator();
        while(iterator.hasNext()) {
            Comic setElement = iterator.next();
            if(setElement.getAuthor().equals(author)) {
                iterator.remove();
            }
        }

        // java 8
        // comics.removeIf(setElement -> setElement.getAuthor().equals(author));
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping author->comics. Map keys are names of the authors (String) present in the library. Map values are
     * collection (e.g.: HashSet<Comic>) of comics for specified author.
     */
    public Map<String, Collection<Comic>> getAuthorsComics() {
        Map<String, Collection<Comic>> result = new HashMap<>();


        for(Comic comic : comics) {
            if(!result.containsKey(comic.getAuthor())) {
                result.put(comic.getAuthor(), new HashSet<>());
            }
        }

        for(Comic comic : comics) {
            result.get(comic.getAuthor()).add(comic);
        }

        return result;
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping publish year->comics. Map keys are publish year (Integer, generics can not be simple types
     * so instead int the Integer is used) present in the library. Map values are collection (e.g.: HashSet<Comic>)
     * of comics for specified author.
     */
    public Map<Integer, Collection<Comic>> getYearsComics() {
        Map<Integer, Collection<Comic>> result = new HashMap<>();


        for(Comic comic : comics) {
            if(!result.containsKey(comic.getPublishYear())) {
                result.put(comic.getPublishYear(), new HashSet<>());
            }
        }

        for(Comic comic : comics) {
            result.get(comic.getPublishYear()).add(comic);
        }

        return result;
    }

    /**
     * Creates specified map and returns it
     * @return Maping year and month -> comic
     */
    public Map<Pair<Integer, Integer>, Collection<Comic>> getYearsMonths() {
        Map<Pair<Integer, Integer>, Collection<Comic>> result = new HashMap<>();


        for(Comic comic : comics) {
            if(!result.containsKey(new MutablePair<>(comic.getPublishYear(), comic.getPublishMonth()))) {
                result.put(new MutablePair<>(comic.getPublishYear(), comic.getPublishMonth()), new HashSet<>());
            }
        }

        for(Comic comic : comics) {
            result.get(new MutablePair<>(comic.getPublishYear(), comic.getPublishMonth())).add(comic);
        }

        return result;
    }
}
